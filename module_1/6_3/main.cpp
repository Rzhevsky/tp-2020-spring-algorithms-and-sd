// 6_3. Реализуйте стратегию выбора опорного элемента “случайный элемент”.
//  Функцию Partition реализуйте методом прохода двумя итераторами от начала массива к концу.

#include <iostream>
#include <algorithm>
#include <ctime>
#include <random>

using std::swap;
using std::cout;
using std::cin;

template<class T>
class isLessOrEqualDefaultFunctor {
public:
    bool operator()(const T& l, const T& r) {
        return l <= r;
    }
};

template <class T, class Compare>
int partition(T * arr, int l, int r, Compare isLessOrEqual) {
    T pivot = arr[r];

    // для любого индекса k справедливо следующее:
    // 1. если l <= k <= i, то arr[k] <= pivot;
    // 2. если i + 1 <= k <= j - 1, то arr[k] > pivot;
    // 3. если k == r, то arr[k] = pivot
    // 4 j <= k <= r - 1 -- все остальные не тронутые элементы массива
    int i = l - 1;
    for (int j = l; j < r; ++j) {
        if (isLessOrEqual(arr[j], pivot)) {
            ++i;
            // если найден элемент не бОльший опорного, то свап
            swap(arr[i], arr[j]);
        }
    }
    swap(arr[i + 1], arr[r]);
    return i + 1;
}

template <class T, class Compare>
int randomizedPartiotion(T * arr, int l, int r, Compare isLessOrEqual) {

    std::random_device rd;     // only used once to initialise (seed) engine
    std::mt19937 rng(rd());    // random-number engine used (Mersenne-Twister in this case)
    std::uniform_int_distribution<int> uni(l, r); // guaranteed unbiased

    // берется случайный индекс из диапазона [l, r]
    int random = uni(rng);

    // опорный случайный элемент меняется с последний эл-том массива
    swap(arr[r], arr[random]);
    return partition(arr, l, r, isLessOrEqual);
}

template <class T, class Compare = isLessOrEqualDefaultFunctor<T>>
int findKStat(T * arr, int l, int r, int k, Compare isLessOrEqual = Compare()) {
    int pivotPos = -1;
    int begin = l;
    int end = r;

    while (pivotPos != k) {
        pivotPos = randomizedPartiotion(arr, begin, end, isLessOrEqual);

        if (pivotPos < k) {
            begin = pivotPos + 1;
        } else {
            end = pivotPos - 1;
        }
    }

    return arr[pivotPos];
}

int main() {

    int n = 0;
    int k = 0;
    cin >> n >> k;

    int * arr = new int[n];
    for (size_t i = 0; i < n; ++i) {
        cin >> arr[i];
    }

    cout << findKStat(arr, 0, n - 1, k);

    delete [] arr;

    return 0;
}
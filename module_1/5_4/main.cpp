// На числовой прямой окрасили N отрезков.
// Известны координаты левого и правого концов каждого отрезка [Li, Ri]. Найти сумму длин частей числовой прямой, окрашенных ровно в один слой.
// N ≤ 10000. Li, Ri — целые числа в диапазоне [0, 109].

#include <iostream>

template <class T>
class isLessDefaultFunctor {
public:
    bool operator() (T& l, T& r) {
        return l < r;
    }
};

template <class T, class Compare>
void merge(T * arr, int left, int middle, int right, Compare isLess);

template <class T, class Compare>
void mergeSort(T * arr, int left, int right, Compare isLess);

template <class T, class Compare = isLessDefaultFunctor<T>>
T sum_of_numeric_line_painted_in_one_layer(T * leftPoints, T * rightPoints, int numOfSegments, Compare isLess = Compare());


int main() {

    int n = 0;
    std::cin >> n;

    // arrays of coordinates of beginning or ending of the segments
    int * leftPoints = new int[n];
    int * rightPoints = new int[n];

    for (int i = 0; i < n; ++i) {
        std::cin >> leftPoints[i];
        std::cin >> rightPoints[i];
    }

    int sum = sum_of_numeric_line_painted_in_one_layer(leftPoints, rightPoints, n);

    std::cout << sum << std::endl;

    delete [] leftPoints;
    delete [] rightPoints;

    return 0;
}

template <class T, class Compare>
T sum_of_numeric_line_painted_in_one_layer(T * leftPoints, T * rightPoints, int numOfSegments, Compare isLess) {
    mergeSort(leftPoints, 0, numOfSegments - 1, isLess);
    mergeSort(rightPoints, 0, numOfSegments - 1, isLess);

    // array of all points
    T * mergeArray = new T[2 * numOfSegments];

    // fill in mergeArray so that invariant for mergesort is preserved
    // (in first half of mergeArray the coordinates of beginning of segments,
    // in second half - coordinates of the ends)
    std::copy(leftPoints, leftPoints + numOfSegments, mergeArray);
    std::copy(rightPoints, rightPoints + numOfSegments, mergeArray + numOfSegments);

    // apply the merge operation to mergeArray (O(n)),
    // mergeArray will be sorted
    merge(mergeArray, 0, numOfSegments - 1, numOfSegments * 2 - 1, isLess);

    int depth = 0;
    int sum = 0;

    int leftIterator = 0;
    int rightIterator = 0;

    for (int k = 0; k < numOfSegments * 2; ++k) {
        // calculate the sum only where depth is 1
        if (depth == 1) {
            sum += mergeArray[k] - mergeArray[k - 1];
        }
        // if there is a starting point of the segment - increase the depth
        if (mergeArray[k] == leftPoints[leftIterator]) {
            ++depth;
            ++leftIterator;
        // if ending point - decrease
        } else if (mergeArray[k] == rightPoints[rightIterator]) {
            --depth;
            ++rightIterator;
        }
    }

    delete [] mergeArray;
    return sum;
}

template <class T, class Compare>
void mergeSort(T * arr, int left, int right, Compare isLess) {
    // borders of blocks
    int leftBorder = 0;
    int rightBorder = 0;
    int midBorder = 0;

    for (int blockSize = 1; blockSize <= right; blockSize *= 2) {
        for (int blockIterator = left; blockIterator <= right - blockSize; blockIterator += 2 * blockSize) {
            int i = 0; // left block iterator
            int j = 0; // right block iterator

            leftBorder = blockIterator;
            midBorder = blockIterator + blockSize;
            rightBorder = blockIterator + 2 * blockSize;
            rightBorder = (rightBorder <= right) ? rightBorder : right + 1;

            T * sortedBlock = new T[rightBorder - leftBorder];

            // while there are elements in both blocks,
            // select the smaller one and put it in the sortedBlock
            while (leftBorder + i < midBorder && midBorder + j < rightBorder) {
                if (isLess(arr[leftBorder + i], arr[midBorder + j])) {
                    sortedBlock[i + j] = arr[leftBorder + i];
                    ++i;
                } else {
                    sortedBlock[i + j] = arr[midBorder + j];
                    ++j;
                }
            }

            // enter the remaining elements from the left or right block into sortedBlock
            while (leftBorder + i < midBorder) {
                sortedBlock[i + j] = arr[leftBorder + i];
                ++i;
            }
            while (midBorder + j < rightBorder) {
                sortedBlock[i + j] = arr[midBorder + j];
                ++j;
            }

            // rewrite source array
            for (int k = 0; k < i + j; ++k) {
                arr[leftBorder + k] = sortedBlock[k];
            }
            delete [] sortedBlock;
        }
    }
}

template <class T, class Compare>
void merge(T * arr, int left, int middle, int right, Compare isLess) {
    if (right <= left) {
        return;
    }

    int n1 = middle - left + 1;  // size of first half of array
    int n2 = right - middle; // size of second half of array

    T * leftArr = new T[n1];
    T * rightArr = new T[n2];

    std::copy(arr + left, arr + left + n1, leftArr);
    std::copy(arr + middle + 1, arr + middle + 1 + n2, rightArr);

    int i = 0;
    int j = 0;

    int k = 0;
    for (k = left; k < right; ++k) {
        if (isLess(leftArr[i], rightArr[j]) && i < n1) {
            arr[k] = leftArr[i++];
        } else if (j < n2){
            arr[k] = rightArr[j++];
        }
    }

    while(i < n1) {
        arr[k] = leftArr[i];
        i++;
        k++;
    }

    while(j < n2) {
        arr[k] = rightArr[j];
        j++;
        k++;
    }

    delete [] leftArr;
    delete [] rightArr;
}

//template <class T, class Compare>
//void mergeSort(T * arr, int left, int right, Compare isLess) {
//    int leftBorder = 0;
//    int rightBorder = 0;
//    int midBorder = 0;
//
//    for (int blockSize = 1; blockSize <= right; blockSize *= 2) {
//        for (int blockIterator = left; blockIterator <= right - blockSize; blockIterator += 2 * blockSize) {
//            int i = 0;
//            int j = 0;
//
//            leftBorder = blockIterator;
//            midBorder = blockIterator + blockSize;
//            rightBorder = blockIterator + 2 * blockSize;
//            rightBorder = (rightBorder <= right) ? rightBorder : right + 1;
//
//            T * sortedBlock = new T[rightBorder - leftBorder];
//
//            while (leftBorder + i < midBorder && midBorder + j < rightBorder) {
//                if (isLess(arr[leftBorder + i], arr[midBorder + j])) {
//                    sortedBlock[i + j] = arr[leftBorder + i];
//                    ++i;
//                } else {
//                    sortedBlock[i + j] = arr[midBorder + j];
//                    ++j;
//                }
//            }
//
//            while (leftBorder + i < midBorder) {
//                sortedBlock[i + j] = arr[leftBorder + i];
//                ++i;
//            }
//            while (midBorder + j < rightBorder) {
//                sortedBlock[i + j] = arr[midBorder + j];
//                ++j;
//            }
//
//            for (int k = 0; k < i + j; ++k) {
//                arr[leftBorder + k] = sortedBlock[k];
//            }
//            delete [] sortedBlock;
//        }
//    }
//}

//template <class T, class Compare>
//T sum_of_numeric_line_painted_in_one_layer(T * leftPoints, T * rightPoints, int size, Compare isLess) {
//    mergeSort(leftPoints, 0, size - 1, isLess);
//    mergeSort(rightPoints, 0, size - 1, isLess);
//
//    T * mergeArray = new T[2 * size];
//    std::copy(leftPoints, leftPoints + size, mergeArray);
//    std::copy(rightPoints, rightPoints + size, mergeArray + size);
//
//    merge(mergeArray, 0, size - 1, 2 * size - 1);
//
//    int depth = 0;
//    int sum = 0;
//
//    int leftIterator = 0;
//    int rightIterator = 0;
//
//    for (int k = 0; k < 2 * size; ++k) {
//        if (depth == 1) {
//            sum += mergeArray[k] - mergeArray[k - 1];
//        }
//
//        if (mergeArray[k] == leftPoints[leftIterator]) {
//            ++depth;
//            ++leftIterator;
//        } else if (mergeArray[k] == rightPoints[rightIterator]) {
//            --depth;
//            ++rightIterator;
//        }
//    }
//
//    delete [] mergeArray;
//    return sum;
//}
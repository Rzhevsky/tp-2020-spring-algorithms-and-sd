//  1_1. Даны два массива целых чисел одинаковой длины A[0..n-1] и B[0..n-1].
//  Необходимо найти первую пару индексов i0 и j0, i0 ≤ j0, такую что A[i0] + B[j0] = max {A[i] + B[j], где 0 <= i < n, 0 <= j < n, i <= j}.
//  Требования: Время работы - O(n).
//  n ≤ 100000.


#include <iostream>
#include <climits>

using std::cin;
using std::cout;

void max_sum(const int * A, const int * B, size_t n, size_t * pair);

#define PAIR 2

int main() {

    size_t n = 0;

    cin >> n;

    int * A = new int[n];
    int * B = new int[n];

    for (size_t i = 0; i < n; ++i) {
        cin >> A[i];
    }

    for (size_t i = 0; i < n; ++i) {
        cin >> B[i];
    }


    size_t pair[PAIR] = {0};

    max_sum(A, B, n, pair);

    cout << pair[0] << " " << pair[1];

    delete [] A;
    delete [] B;

    return 0;
}

void max_sum(const int * A, const int * B, size_t n, size_t * pair) {

    int * max_positions = new int[n];

    // составляется массив из максимальных индексов, по принципу:
    // для i-й позиции в массиве A находится  максимум в диапазоне A[0 : i]
    // и индекс найденного максимума сохраняется в max_positions[i]
    int max = INT_MIN;
    size_t max_index = 0;
    for (size_t i = 0; i < n; ++i) {
        if (A[i] > max) {
            max = A[i];
            max_index = i;
        }
        max_positions[i] = max_index;
    }

    // поиск индексов элементов, образующих максимальную сумму
    int max_of_two = 0;
    for (size_t i = 0; i < n; ++i) {
        max_of_two = B[i] + A[max_positions[i]];
        if ((max_of_two > max) && (max_positions[i] <= i)) {
            max = max_of_two;
            pair[0] = max_positions[i];
            pair[1] = i;
        }
    }

    delete [] max_positions;
}


// Дан отсортированный массив целых чисел A[0..n-1] и массив целых чисел B[0..m-1]. Для
// каждого элемента массива B[i] найдите минимальный индекс k минимального элемента
// массива A, равного или превосходящего B[i]: A[k] >= B[i]. Если такого элемента нет, выведите
// n. Время работы поиска k для каждого элемента B[i]: O(log(k)). n, m ≤ 10000.

#include <iostream>
#include <algorithm>

using std::cout;
using std::cin;
using std::min;

int binSearch(const int * arr, int left, int right, int element);
int algo(const int * A, int n, int element);


int main() {

    int n = 0;
    int m = 0;

    cin >> n >> m;

    int * A = new int[n];
    int * B = new int[m];

    for (size_t i = 0; i < n; ++i) {
        cin >> A[i];
    }

    for (size_t i = 0; i < m; ++i) {
        cin >> B[i];
    }

    for (int i = 0; i < m; ++i) {
        cout << algo(A, n, B[i]) << " ";
    }

    delete [] A;
    delete [] B;

    return 0;
}

int binSearch(const int * arr, int left, int right, int element) {
    int first = left;
    int last = right;

    while (first < last) {
        int mid = (first + last) / 2;

        if (arr[mid] < element) {
            first = mid + 1;
        } else {
            last = mid;
        }
    }

    return element <= arr[last] ? last : right;
}


int algo(const int * A, int n, int element) {
    int l = 0;
    int r = 1;

    while (A[r] < element && r < n) {
        l = r;
        r *= 2;
    }

    r = min(r, n);

    return binSearch(A, l, r, element);
}

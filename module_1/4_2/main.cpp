#include <iostream>
#include <cassert>
#include <cstring>

#define GROWTH_FACTOR 2

using std::cin;
using std::cout;

template <class T>
class DArray {
private:
    void reallocate();

    T* buffer_;
    size_t buffSize_;
    size_t count_;

public:

    DArray& operator=(const DArray& darr) = delete;
    ~DArray();

    DArray(const DArray& darr);

    explicit DArray(size_t buffSize__ = 10);
    void pushBack(const T& elem);
    T popBack();

    T At(size_t index) const;

    T& operator[](size_t index);
    const T& operator[](size_t index) const;

    size_t getSize() const { return count_; };
    size_t getCapacity() const { return buffSize_; };
    bool isEmpty() const { return count_ == 0; };
};

template <class T>
DArray<T>::DArray(const DArray& darr) {
    auto newBuffer = new T[darr.getCapacity()];
    for (size_t i = 0; i < darr.getSize(); ++i) {
        newBuffer[i] = darr[i];
    }
    buffSize_ = darr.getCapacity();
    count_ = darr.getSize();
    buffer_ = newBuffer;
}


template <class T>
DArray<T>::DArray(size_t buffSize__) :
    buffer_(nullptr),
    buffSize_(buffSize__),
    count_(0)
{
    buffer_ = new T[buffSize__];
}

template <class T>
DArray<T>::~DArray() {
    delete[](buffer_);
}

template <class T>
void DArray<T>::reallocate() {
    size_t newBuffgetSize = buffSize_ * GROWTH_FACTOR;
    auto newBuffer = new T[newBuffgetSize];
    memcpy(newBuffer, buffer_, buffSize_ * sizeof(T));

    delete[](buffer_);
    buffSize_ = newBuffgetSize;
    buffer_ = newBuffer;
}

template <class T>
void DArray<T>::pushBack(const T& elem) {
    if (count_ == buffSize_) {
        reallocate();
    }

    buffer_[count_++] = elem;
}

template <class T>
T DArray<T>::popBack() {
    assert(count_);
    return buffer_[--count_];
}

template <class T>
T& DArray<T>::operator[](size_t index) {
    return buffer_[index];
}

template <class T>
const T& DArray<T>::operator[](size_t index) const {
    return buffer_[index];
}

template <class T>
T DArray<T>::At(size_t index) const {
    assert(buffer_ && (index < buffSize_));

    return buffer_[index];
}


// comparator
template<class T>
class IsLessDefaultFunctor {
public:
    bool operator()(const T& l, const T& r)
    {
        return l < r;
    }
};

// HEAP

template<class T, class Compare = IsLessDefaultFunctor<T>>
class Heap {
private:
    DArray<T> buffer_;
    int heapSize_;
    int count_;

    void buildMinHeap();

    void minHeapify(int i, Compare isLess = Compare());

    void siftUp(int i, Compare isLess = Compare());

    void swap(T& a, T& b);

public:

    Heap() :
        buffer_(),
        heapSize_(buffer_.getCapacity()),
        count_(0) {};

    explicit Heap(DArray<T>& darray, int size) :
        buffer_(darray),
        heapSize_(size),
        count_(size) { buildMinHeap(); };

    Heap(const Heap& heap) = delete;
    Heap& operator=(const Heap& heap) = delete;
    ~Heap();

    // Размер кучи
    [[nodiscard]] int getSize() const { return buffer_.getSize(); };

    // Добавить элемент O(log n)
    void addElement(const T& elem);

    // Извлечь минимум O(log n)
    T extractMin();

    void printHeap();

};

template<class T, class Compare>
void Heap<T, Compare>::minHeapify(int i, Compare isLess) {
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    int smallest = i;
    if (left < count_ && isLess(buffer_[left], buffer_[i])) {
        smallest = left;
    }
    if (right < count_ && isLess(buffer_[right], buffer_[smallest])) {
        smallest = right;
    }

    if (smallest != i) {
        swap(buffer_[i], buffer_[smallest]);
        minHeapify(smallest);
    }
}

template<class T, class Compare>
void Heap<T, Compare>::swap(T & a, T & b) {
    T tmp = a;
    a = b;
    b = tmp;
}

template<class T, class Compare>
void Heap<T, Compare>::buildMinHeap() {
    for (int i = heapSize_ / 2 - 1; i >= 0; --i) {
        minHeapify(i);
    }
}

template<class T, class Compare>
void Heap<T, Compare>::addElement(const T & elem) {
    buffer_.pushBack(elem);
    siftUp(count_++);
}


template<class T, class Compare>
void Heap<T, Compare>::siftUp(int i, Compare isLess) {
    while (i > 0) {
        int parent = (i - 1) / 2;
        if (!isLess(buffer_[i], buffer_[parent])) {
            return;
        }
        swap(buffer_[i], buffer_[parent]);
        i = parent;
    }
}


template<class T, class Compare>
T Heap<T, Compare>::extractMin() {
    assert(!buffer_.isEmpty());

    T res = buffer_.At(0);

    buffer_.popBack();
    swap(buffer_[0], buffer_[--count_]);

    minHeapify(0);
    return res;
}

template<class T, class Compare>
Heap<T, Compare>::~Heap() {
}

template<class T, class Compare>
void Heap<T, Compare>::printHeap() {
    assert(!buffer_.isEmpty());

    for (int i = 0; i < count_; ++i) {
        cout << buffer_[i] << " ";
    }
    cout << std::endl;
}


template<class T>
T countTime(Heap<T>& heap);

int main() {

    int n = 0;
    cin >> n;

    DArray<int> darr;

    int val = 0;
    for (int i = 0; i < n; ++i) {
        cin >> val;
        darr.pushBack(val);
    }

    Heap heap(darr, n);

    cout << countTime(heap);

    return 0;
}


template<class T>
T countTime(Heap<T>& heap) {
    if (!heap.getSize()) {
        return 0;
    }

    T res = 0;
    T sum = 0;

    while (heap.getSize() > 1) {
        sum = heap.extractMin() + heap.extractMin();

        heap.addElement(sum);

        res += sum;
    }

    return res;
}

#include <iostream>
#include <cassert>
#include <cstring>

#define POP_FRONT 2
#define PUSH_BACK 3

#define GROWTH_FACTOR 2
#define DEFAULT_SIZE 10

template <class T>
class Stack {
private:
    T* buffer_;
    size_t buffSize_;
    size_t count_;

    void reallocate();

public:
    Stack() :
        buffer_(nullptr),
        buffSize_(DEFAULT_SIZE),
        count_(0)
        {
            buffer_ = new T[buffSize_];
        } ;


    Stack(const Stack& q) = delete;
    Stack& operator=(const Stack& q) = delete;

    ~Stack() { delete [] buffer_; };

    T pop();
    void push(const T& elem);

    bool isEmpty() const { return count_ == 0; };
    size_t size() const { return count_; };

};

template <class T>
void Stack<T>::reallocate() {
    T* newBuffer = new T[buffSize_ * GROWTH_FACTOR];
    memcpy(newBuffer, buffer_, buffSize_ * sizeof(T));

    delete [] buffer_;

    buffer_ = newBuffer;
    buffSize_ *= GROWTH_FACTOR;
}


template <class T>
void Stack<T>::push(const T& elem) {

    if (count_ == buffSize_) {
        reallocate();
    }

    buffer_[count_++] = elem;
}


template <class T>
T Stack<T>::pop() {
    assert(count_);

    return buffer_[--count_];
}


template <class T>
class Queue {
private:
    Stack<T> left_;
    Stack<T> right_;

public:
    Queue() :
        left_(),
        right_() {};

    Queue(const Queue& q) = delete;
    Queue& operator=(const Queue& q) = delete;
    ~Queue() = default;

    void enqueue(const T& elem);

    T dequeue();

    bool isEmpty() const { return left_.isEmpty() && right_.isEmpty(); };

};


template <class T>
void Queue<T>::enqueue(const T& elem) {
    right_.push(elem);
}

template <class T>
T Queue<T>::dequeue() {
    if(isEmpty()) {
        return -1;
    }

    if (left_.isEmpty()) {
        T len = right_.size();
        for (size_t i = 0; i < len; ++i) {
            left_.push(right_.pop());
        }
    }

    return left_.pop();
}


void run(std::istream& input, std::ostream& output) {
    Queue<int> q;
    int n = 0;
    input >> n;

    bool result = true;
    for (int i = 0; i < n && result; ++i) {
        int command = 0;
        int data = 0;

        input >> command >> data;

        switch (command) {
            case POP_FRONT:
                if (q.isEmpty()) {
                    result = result && data == -1;
                } else {
                    result = result && data == q.dequeue();
                }
                break;

            case PUSH_BACK:
                q.enqueue(data);
                break;

            default:
                assert(false);
        }
    }

    output << (result ? "YES" : "NO");
}

int main() {

    run(std::cin, std::cout);

    return 0;
}
